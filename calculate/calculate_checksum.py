#!/usr/bin/env python3

from hashlib import md5, sha1, sha224, sha384, sha256, sha512


class Checksum(object):
    def __init__(self, file, chunk=1048576):
        self.chunk = chunk  # Default size 1MB chunk
        self.file = file

    def md5_hash(self):
        """
        Calculate the MD5 hash checksum
        """
        return self.calculate_hash(hash_type=md5())

    def sha1_hash(self):
        """
        Calculate the SHA1 hash checksum
        """
        return self.calculate_hash(hash_type=sha1())

    def sha224_hash(self):
        """
        Calculate the SHA224 hash checksum
        """
        return self.calculate_hash(hash_type=sha224())

    def sha256_hash(self):
        """
        Calculate the SHA256 hash checksum
        """
        return self.calculate_hash(hash_type=sha256())

    def sha384_hash(self):
        """
        Calculate the SHA384 hash checksum
        """
        return self.calculate_hash(hash_type=sha384())

    def sha512_hash(self):
        """
        Calculate the SHA512 hash checksum
        """
        return self.calculate_hash(hash_type=sha512())

    def calculate_hash(self, hash_type):
        """
        Opens the file and reads it in 1MB chunks, unless specified when the object is first created
        :param hash_type: hash type object
        :return: hash checksum
        """
        try:
            with open(self.file, "rb") as f:
                working_chunk = f.read(self.chunk)
                while working_chunk:
                    hash_type.update(working_chunk)
                    working_chunk = f.read(self.chunk)

            return hash_type.hexdigest()

        except (FileNotFoundError, EOFError, IOError) as e:
            return e


if __name__ == '__main__':
    app = Checksum(file=None)
