#!/usr/bin/env python3
########################################################################################################################
# Author: David Cartwright
# Date: 16 October 2017
# Version: 0.1
#
# This software is the provides a GUI to verify the checksum of a file.
#
# Changelog
# =========
#
# 0.1 29/09/2017 - Initial Release
########################################################################################################################

# Import Modules
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from calculate.calculate_checksum import Checksum

# Software Information
VERSION = "0.1"
TITLE = "Checksum File Check"


class InputFrame(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)

        # Variables
        self.filename = StringVar()
        self.hash_type = StringVar()

        # Widgets
        self.input_frm = ttk.LabelFrame(self, text="File Input: ")
        self.frame1 = Frame(self.input_frm)
        self.frame1_lbl_1 = ttk.Label(self.frame1, text="File: ")
        self.filename_entry = ttk.Entry(self.frame1, textvariable=self.filename, width=60)
        self.open_btn = ttk.Button(self.frame1, text="Open", command=self.open_file)
        self.option_frm = ttk.LabelFrame(self.input_frm, text="Checksum Options: ")
        self.md5_checksum = ttk.Radiobutton(self.option_frm,
                                            text="MD5",
                                            variable=self.hash_type,
                                            value="md5")
        self.sha1_checksum = ttk.Radiobutton(self.option_frm,
                                             text="SHA1",
                                             variable=self.hash_type,
                                             value="sha1")
        self.sha224_checksum = ttk.Radiobutton(self.option_frm,
                                               text="SHA224",
                                               variable=self.hash_type,
                                               value="sha224")
        self.sha256_checksum = ttk.Radiobutton(self.option_frm,
                                               text="SHA256",
                                               variable=self.hash_type,
                                               value="sha256")
        self.sha384_checksum = ttk.Radiobutton(self.option_frm,
                                               text="SHA384",
                                               variable=self.hash_type,
                                               value="sha384")
        self.sha512_checksum = ttk.Radiobutton(self.option_frm,
                                               text="SHA512",
                                               variable=self.hash_type,
                                               value="sha512")

        # Position Widgets
        self.input_frm.pack(side=TOP, fill=BOTH)
        self.frame1.pack(side=TOP, fill=BOTH)
        self.frame1_lbl_1.pack(side=LEFT)
        self.filename_entry.pack(side=LEFT)
        self.open_btn.pack(side=RIGHT)
        self.option_frm.pack(side=BOTTOM, fill=BOTH)
        self.md5_checksum.grid(row=0, column=0, sticky=W + E)
        self.sha1_checksum.grid(row=0, column=1, sticky=W + E)
        self.sha224_checksum.grid(row=0, column=2, sticky=W + E)
        self.sha256_checksum.grid(row=0, column=3, sticky=W + E)
        self.sha384_checksum.grid(row=0, column=4, sticky=W + E)
        self.sha512_checksum.grid(row=0, column=5, sticky=W + E)

        # Add some padding
        for child in self.input_frm.winfo_children():
            child.pack_configure(padx=5, pady=5)

        for child in self.frame1.winfo_children():
            child.pack_configure(padx=5)

        for child in self.option_frm.winfo_children():
            child.grid_configure(padx=12, pady=5)

    def open_file(self):
        # Get the file name information.
        #
        if len(self.filename_entry.get()) > 0:
            self.filename.set('')

        self.filename_entry.insert(0, askopenfilename())


class OutputFrame(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)

        # Variables
        self.checksum = StringVar()

        # Widgets
        self.output_frm = ttk.LabelFrame(self, text="Checksum: ")
        self.output_frm_lbl = ttk.Label(self.output_frm, text="Checksum: ")
        self.checksum_entry = ttk.Entry(self.output_frm, textvariable=self.checksum, width=70)

        # Position Widgets
        self.output_frm_lbl.pack(side=LEFT)
        self.output_frm.pack(side=TOP, fill=BOTH)
        self.checksum_entry.pack(side=LEFT)

        # Add some padding
        for child in self.output_frm.winfo_children():
            child.pack_configure(padx=5, pady=5)


class ButtonFrames(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)

        ttk.Button(self, text="Quit", command=shutdown).pack(side=RIGHT, padx=5)
        ttk.Button(self, text="Calculate", command=calculate).pack(side=LEFT, padx=5)


class App(Frame):
    """
    The main application frame.
    """
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.input_frame = InputFrame(root)
        self.output_frame = OutputFrame(root)
        self.button_frame = ButtonFrames(root)

        self.button_frame.pack(side=BOTTOM, padx=5, pady=5)
        self.input_frame.pack(side=TOP, fill=BOTH, padx=5, pady=5)
        self.output_frame.pack(side=BOTTOM, fill=BOTH, padx=5, pady=5)


def right_click(entry):
    """
    Right click context menu for checksum entry box
    """
    try:
        # For added ease, let's select all and copy with one click
        def copy(entry):
            entry.widget.event_generate('<Control-a>')
            entry.widget.event_generate('<Control-c>')

        entry.widget.focus()

        rmenu = Menu(None, tearoff=0, takefocus=0)
        rmenu.add_command(
            label="Copy to Clipboard",
            command=lambda entry=entry: copy(entry))
        rmenu.tk_popup(
            entry.x_root + 40,
            entry.y_root + 10,
            entry="0")

    except TclError:
        pass


def shutdown():
    """
    Exit the program
    """
    sys.exit(0)


def calculate():
    """
    Creates the file_checksum object and passes the file name and hash type to the Checksum class.
    """
    file_name = app.input_frame.filename_entry.get()
    if len(file_name) > 0:
        file_checksum = Checksum(file=file_name)

        if app.input_frame.hash_type.get() == "md5":
            app.output_frame.checksum.set(file_checksum.md5_hash())
        elif app.input_frame.hash_type.get() == "sha1":
            app.output_frame.checksum.set(file_checksum.sha1_hash())
        elif app.input_frame.hash_type.get() == "sha224":
            app.output_frame.checksum.set(file_checksum.sha224_hash())
        elif app.input_frame.hash_type.get() == "sha256":
            app.output_frame.checksum.set(file_checksum.sha256_hash())
        elif app.input_frame.hash_type.get() == "sha384":
            app.output_frame.checksum.set(file_checksum.sha384_hash())
        elif app.input_frame.hash_type.get() == "sha512":
            app.output_frame.checksum.set(file_checksum.sha512_hash())
        else:
            app.output_frame.checksum.set("Error: Unknown hash type")


if __name__ == '__main__':
    # Main Window
    #
    root = Tk()
    root.title(TITLE + " " + VERSION)

    app = App(root)
    app.pack(side=TOP, fill=BOTH)
    root.resizable(0, 0)

    # Bind the right-click menu to the checksum entry widget
    app.output_frame.checksum_entry.bind('<Button-3>', right_click, add='')

    root.mainloop()
